var express = require('express');
var app = express();
var server = require('http').createServer(app);
var socket = require('socket.io')(server);
var dbHelper = require('./helpers/dbHelper');
var roomRoute = require('./routes/rooms');
var bodyParser = require('body-parser');
var path = require('path');

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use('/rooms', roomRoute);
app.use('/', express.static(path.join(__dirname, 'public')));
app.use('/videos', express.static('uploads'));

socket.on('connection', function (client) {
  console.log("[New Client] -- "+ client.request.connection.remoteAddress);
  client.on('join', function (msg) {
    console.log("[Info] Client " + client.request.connection.remoteAddress + "@" + msg.name + " joined to room " + msg.number);
    client.join(msg.number);
  });
  client.on('changetime', function (msg) {
    console.log("[Info-ChangeTime] Broadcast // Channel: " + msg.id + " // Time: " + msg.time);
    console.log(msg);
    client.broadcast.to(msg.id).emit('changetime', {'time': msg.time, 'msg': msg.msg, 'user': msg.user, 'ev': msg.ev});
  });
  client.on('play', function (msg) {
    console.log("[Info-Play] Broadcast // Channel: " + msg.id + " // Time: " + msg.time);
    client.broadcast.to(msg.id).emit('play', {'time': msg.time });
  });
  client.on('pause', function (msg) {
    console.log("[Info-Pause] Broadcast // Channel: " + msg.id + " // Time: " + msg.time);
    client.broadcast.to(msg.id).emit('pause', {'time': msg.time, 'msg': msg.msg, 'user': msg.user, 'ev': msg.ev});
  });
  client.on('message', function (msg) {
    console.log("[Info-Message] Broadcast // Channel: " + msg.id + " // Time: " + msg.time);
    console.log(msg);
    client.broadcast.to(msg.id).emit('message', {'time': msg.time, 'msg': msg.msg, 'user': msg.user, 'ev': msg.ev});
  });

});

server.listen(3000, function () {
  console.log("Service started.");
});
