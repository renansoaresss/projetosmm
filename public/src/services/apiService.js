app.factory('apiService', function ($http, config) {

  function _getRooms() {
    return $http.get(config.apiURL+'/rooms');
  }

  var _addRoom = function (room, file, callback) {

    var fd = new FormData();

    var url = "/rooms";

    fd.append("file", file);
    fd.append("name", room.name);

    return ($http.post(config.apiURL+url, fd, {
      withCredentials : false,
      headers : {
        'Content-Type' : undefined
      },
      transformRequest : angular.identity
    }));
  };

  function _deleteRoom(id) {
    return $http.delete(config.apiURL+'/rooms/'+id);
  }

  return {
    getRooms: _getRooms,
    addRoom: _addRoom,
    deleteRoom: _deleteRoom
  }

});
