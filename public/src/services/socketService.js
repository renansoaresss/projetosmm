app.factory('socketService', function ($rootScope) {
  console.log("oi");
  var socket = io.connect('http://localhost:3000');
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      console.log("socketEmit");
      socket.emit(eventName, data, function () {
        // var args = arguments;
        // $rootScope.$apply(function () {
        //   if (callback) {
        //     callback.apply(socket, args);
        //   }
        // });
      })
    }
  };
});
