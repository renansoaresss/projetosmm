var app = angular.module('projetosmm', ['ui.router', 'ui.bootstrap'])

.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

  $stateProvider
  .state('app', {
      abstract: true,
      templateUrl: '/index.html'
  })
  .state('home', {
    url: '/home',
    templateUrl: 'src/templates/home.html',
    controller: 'homeCtrl',
  })
  .state('homeUser', {
    url: '/videoUser',
    templateUrl: 'src/templates/videoUser.html',
    controller: 'homeUserCtrl'
  });

  $urlRouterProvider.otherwise('/home');

});
