app.controller("homeCtrl", function ($scope, $rootScope, $state, $uibModal, socketService, apiService, config) {

  $scope.roomSelected = false;
  $scope.rooms = [];

  $scope.setRoom = function(id) {
    setRoom(id);
  }

  $scope.newRoom = function() {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'src/templates/_modalRoom.html',
      controller: 'modalVideoCtrl'
    });
  }

  $scope.deleteRoom = function(id) {
    var room = $scope.rooms.filter(function (room) {
      return room.id === id;
    });
    apiService.deleteRoom(id)
    .success(function (result) {
      $scope.rooms.splice($scope.rooms.indexOf(room[0]), 1);
      alert("Sala removida.")
    })
    .error(function (result) {
      console.log(result);
      alert("Ocorreu um erro.");
    });
  }

  function setRoom(room) {
      $rootScope.room = room;
      $state.go('homeUser');
  }

  function listRooms() {
    apiService.getRooms()
    .success(function (data) {
      $scope.rooms = data;
    })
    .error(function (data) {
      console.log("Erro ao listar salas.");
    });
  }

  listRooms();

});
