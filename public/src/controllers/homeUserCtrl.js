app.controller("homeUserCtrl", function ($sce, $scope, $rootScope, apiService, socketService, config) {

  $scope.room = $rootScope.room;
  $scope.name = null;
  $scope.messages = [];
  $scope.newMessage = {};

  var video = document.getElementById("video");

	// Buttons
	var playButton = document.getElementById("play-pause");
	var muteButton = document.getElementById("mute");
	var fullScreenButton = document.getElementById("full-screen");

	// Sliders
	var seekBar = document.getElementById("seek-bar");
	var volumeBar = document.getElementById("volume-bar");


	// Event listener for the play/pause button
	playButton.addEventListener("click", function() {
		if (video.paused == true) {
			// Play the video
			video.play();
      socketService.emit('play', { id: $scope.room.id, time: vid.currentTime });

			// Update the button text to 'Pause'
			playButton.innerHTML = "Pause";
		} else {
      $scope.msg = prompt("Motivo do pause: ", "");
			// Pause the video
      socketService.emit('pause', { id: $scope.room.id, time: vid.currentTime, user: $scope.name, msg: $scope.msg, ev: 'Pausa'});
      $scope.messages.push({time: vid.currentTime, user: $scope.name, msg: $scope.msg, ev: 'Pausa'});
      $scope.$apply();
			video.pause();

			// Update the button text to 'Play'
			playButton.innerHTML = "Play";
		}
	});


	// Event listener for the mute button
	muteButton.addEventListener("click", function() {
		if (video.muted == false) {
			// Mute the video
			video.muted = true;

			// Update the button text
			muteButton.innerHTML = "Unmute";
		} else {
			// Unmute the video
			video.muted = false;

			// Update the button text
			muteButton.innerHTML = "Mute";
		}
	});


	// Event listener for the full-screen button
	fullScreenButton.addEventListener("click", function() {
		if (video.requestFullscreen) {
			video.requestFullscreen();
		} else if (video.mozRequestFullScreen) {
			video.mozRequestFullScreen(); // Firefox
		} else if (video.webkitRequestFullscreen) {
			video.webkitRequestFullscreen(); // Chrome and Safari
		}
	});


	// Event listener for the seek bar
	seekBar.addEventListener("change", function() {
		// Calculate the new time
    $scope.msg = prompt("Motivo da mudança no tempo: ", "");
    socketService.emit('changetime', { id: $scope.room.id, time: vid.currentTime, user: $scope.name, msg: $scope.msg, ev: 'Mudança no tempo'});
    $scope.messages.push({time: vid.currentTime, user: $scope.name, msg: $scope.msg, ev: 'Mudança no tempo'});
    $scope.$apply();
		var time = video.duration * (seekBar.value / 100);

		// Update the video time
		video.currentTime = time;
	});


	// Update the seek bar as the video plays
	video.addEventListener("timeupdate", function() {
		// Calculate the slider value
		var value = (100 / video.duration) * video.currentTime;

		// Update the slider value
		seekBar.value = value;
	});

	// Pause the video when the seek handle is being dragged
	// seekBar.addEventListener("mousedown", function() {
	// 	video.pause();
	// });
  //
	// // Play the video when the seek handle is dropped
	// seekBar.addEventListener("mouseup", function() {
	// 	video.play();
	// });

	// Event listener for the volume bar
	volumeBar.addEventListener("change", function() {
		// Update the video volume
		video.volume = volumeBar.value;
	});

  var vid = document.getElementById("video");

  // vid.onseeked = function () { onTimeUpdate() };
  // vid.onplay = function () { onPlay() };
  // vid.onpause = function() { onPause() };

  socketService.on('changetime', function (data) {
    console.log("recebeu change time");
    console.log(data);
    vid.currentTime = data.time;
    $scope.messages.push({user: data.user, msg: data.msg, time: data.time});
    var value = (100 / vid.duration) * vid.currentTime;
		seekBar.value = value;
  });

  socketService.on('play', function (data) {
    vid.currentTime = data.time;
    vid.play();
  });

  socketService.on('pause', function (data) {
    vid.currentTime = data.time;
    vid.pause();
    $scope.messages.push({user: data.user, msg: data.msg, time: data.time});
  });

  socketService.on('message', function (data) {
    console.log("chegou mensagem");
    $scope.messages.push(data);
    $scope.$apply();
  });

  $scope.getRoomURL = function(videoFile) {
    return ($sce.trustAsResourceUrl(config.apiURL+'/videos/'+videoFile));
  }

  $scope.sendMessage = function() {
    socketService.emit('message', { id: $scope.room.id, time: vid.currentTime, user: $scope.name, msg: $scope.newMessage.msg, ev: 'Mensagem'});
    $scope.messages.push({user: $scope.name, msg: $scope.newMessage.msg, time: vid.currentTime, ev: 'Mensagem'});
    $scope.newMessage.msg = "";
    // $scope.$apply();
  }

  function enterRoom() {
    while ($scope.name === null) {
      $scope.name = prompt("Qual seu nome?", "");
    }
    socketService.emit('join', { number: $rootScope.room.id, name: $scope.name });
  }

  enterRoom();

  // function onTimeUpdate() {
  //   socketService.emit('changetime', { id: $scope.room.id, time: vid.currentTime, user: $scope.name, msg: $scope.msg});
  // }
  //
  // function onPlay() {
  //   socketService.emit('play', { id: $scope.room.id, time: vid.currentTime });
  // }
  //
  // function onPause() {
  //   socketService.emit('pause', { id: $scope.room.id, time: vid.currentTime, user: $scope.name, msg: $scope.msg });
  // }

});
