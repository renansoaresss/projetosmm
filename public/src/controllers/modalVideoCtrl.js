app.controller("modalVideoCtrl", function ($scope, $rootScope, $uibModalStack, $state, apiService, socketService, config) {

  $scope.room = {};

  $scope.uploadFile = function(file) {
    $scope.$apply(function () {
      $scope.file = file.files[0];
    });
  };

  $scope.addRoom = function () {
    apiService.addRoom($scope.room, $scope.file)
    .success(function (result) {
      $rootScope.room = result;
      alert("Sala criada com sucesso.");
      $uibModalStack.dismissAll();
      $state.go('homeUser');
    })
    .error(function (result) {
      alert("Houve um erro ao criar uma nova sala.");
    });
  }

});
