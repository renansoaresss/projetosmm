var fs = require('fs');

function fileRename(file) {
  fs.renameSync(file.destination+file.filename, file.destination+file.originalname);
}

module.exports.fileRename = fileRename;
