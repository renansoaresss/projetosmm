var mongoose = require('mongoose');
require('../db/config').connect(mongoose);
var Room = require('../db/schemas/Room').init(mongoose);
var db = require('../db/api');

function addRoom(id, name, file, callback) {
  var new_room = new Room({
    id: id,
    name: name,
    file: file
  });

  db.create(new_room, function (result) {
    callback();
  });
}

function listRooms(callback) {
  db.read_all(Room, function (result) {
    callback(result);
  });
}

function removeRoom(id, callback) {
  db.remove(Room, id, function (result) {
    callback(result);
  });
}

module.exports.addRoom = addRoom;
module.exports.listRooms = listRooms;
module.exports.removeRoom = removeRoom;
