var express = require('express');
var multer  = require('multer')
var dbHelper = require('../helpers/dbHelper')
var fileHelper = require('../helpers/fileHelper');
var router = express.Router();
var upload = multer({ dest: 'uploads/' });
var uuid = require('node-uuid');

router.route('/')

.post(upload.single('file'), function (req, res) {
  console.log(req.body);
  console.log(req.file);
  fileHelper.fileRename(req.file);
  var id = uuid.v4();
  dbHelper.addRoom(id, req.body.name, req.file.originalname, function (result) {
    res.send(200, { info: 'new room added!', uuid: id, name: req.body.name, file: req.file.originalname });
  });
})

.get(function (req, res) {
  dbHelper.listRooms(function (rooms) {
    res.json(rooms);
  });
});

router.route('/:id')

.delete(function (req, res) {
  console.log(req.params['id']);
  dbHelper.removeRoom(req.params['id'], function (room) {
    res.status(200).end();
  });
});

module.exports = router;
