function init_schema(mongoose) {
 	var roomSchema = new mongoose.Schema({
	  id: String,
	  name: String,
	  file: String,
	  created_at: { type: Date },
	  updated_at: { type: Date },
	});

	var Room = mongoose.model('Room', roomSchema);

	return Room;

};

module.exports.init = init_schema;
